import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
})
export class UsersComponent implements OnInit {
  page = 1;
  pageSize = 10;
  items = [];
  constructor() {
    for (let i = 1; i <= 100; i++) {
      this.items.push({ name: 'User ' + i });
    }
  }

  ngOnInit(): void {}
}
